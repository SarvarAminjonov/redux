export const getMovieId = (id) => {
    return {
        type: 'MOVIE_ID',
        payload: id
    }
}

// export const getAllMovies = (value) => {
//     return {
//         type: 'MOVIE_DATA',
//         payload: value
//     }
// }

// export const getAllComments = (value) => {
//     return {
//         type: 'COMMENTS_DATA',
//         payload: value
//     }
// }
