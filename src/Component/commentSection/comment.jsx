import React from "react";
import "./comment.css"
import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import axios from "axios";

const Comment = () =>{
    const movieId = useSelector((state) => state.movie.movieId);
    console.log(movieId);
    const [comments, setComments] = useState([]);
  
    const inputsValues = {
      username: "",
      comment_msg: "",
    };
    const [values, setValues] = useState(inputsValues);
    console.log(values);
  
    const getComments = () => {
      axios
        .get(
          `https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/movies/${movieId}/comments`
        )
        .then((res) => setComments(res.data));
    };
  
    const handleSubmit = (e) => {
      e.preventDefault();
  
      axios
        .post(
          `https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/movies/${movieId}/comments`,
          values
        ) 
        .then((res) => console.log(res.data))
        .finally(() => {
          getComments();
        });
    };
  
    useEffect(() => {
      getComments();
    }, [movieId]);
    


    return(
        <div className="comment-box">
            <div className="comHead">
                <p>Comments</p>
            </div>
            <div className="comBody">
                {comments.map((comment) => (
                <p key={comment?.id}>
                <span className="s">{comment?.username}</span> <br /><br/>
                {comment?.comment_msg}
                </p>
            ))}
            </div>
            <div className="comFooter">
            <form className="com-form" onSubmit={handleSubmit}>
                <input type="text" placeholder='Username' value={values.username} required onChange={(e) => setValues({ ...values, username: e.target.value })}/>
                <textarea name="comment"
                 rows="10" placeholder='Comments' 
                 value={values.comment_msg} 
                 required  
                 onChange={(e) => setValues({ ...values, comment_msg: e.target.value })}>
                 </textarea>
               <button className='btnPost' type="submit">POST</button>
            </form>
            </div>
        </div>
    );
}

export default Comment