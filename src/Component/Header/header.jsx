import React from "react";
import './header.css'
import Search from '../icon/Search.svg'
import Circle from '../icon/Circle.svg'
import dots from '../icon/Dots.svg'

const Header = () => {


    return(
        <div className="nav-block">
            <div className="logo-text">
                <p>
                MOVEA
                </p>
            </div>
            <div className="nav-link">
                <ul>
                    <li>Movies</li>
                    <li>TV shows</li>
                    <li>Animations</li>
                </ul>
            </div>
            <div className="nav-icon">
                <div className="icon-box">
                <div className="search">
                    <img src={Search} alt="search" />
                </div>
                <div className="search">
                    <img src={Circle} alt="search" />
                </div>
                <div className="search">
                    <img src={dots} alt="search" />
                </div>
                </div>
                
            </div>
            
        </div>
    )
}

export default Header