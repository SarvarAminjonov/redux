import React from "react";
import {useState, useEffect} from "react"
import axios from "axios";
import './movie.css'
import Play from "../icon/play.svg"
import { useDispatch } from "react-redux";
import { getMovieId } from "../../redux/action/actionReducer";


const Movies = () =>{
    const [movie, setMovie] = useState([]);
    const [banner, setBanner] = useState([]);
    const dispatch = useDispatch();
    const getMovies = () => {
        axios
          .get(`https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/movies`)
          .then((res) => {
            setMovie(res.data);
          });
      };
      const getBanner = () => {
        axios
          .get(`https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/movies`)
          .then((res) => {
            setBanner(res.data.filter((item) => item.type === "banner"));
          });
      };
      useEffect(()=>{
          getMovies();
          getBanner();
      },[]);
      return(
          <>
        <div className="movieSection">
            <div className="banner_section">
                <img src={banner[0]?.movieImageUrl} alt={banner[0]?.title} />
               
            </div>
            <p className="movies-text">Continue Watching  |  4 Movies</p>
          
          <div className="movieWrap">
            {movie.slice(0,4).map((movi) => (
            <div key={movi.id} className = "movie_box" onClick={() => {dispatch(getMovieId(movi?.id))}}>
                     <img src={movi?.movieImageUrl} alt={movi?.title} />
                     <div className="info-box">
                        <div className="playIcon">
                            <img src={Play} alt="" />
                        </div>
                        <div className="info">
                            <p className="mData">{movi.title}</p>
                            <p className="mRelease">{movi.release_date}</p>
                        </div>
                        <div className="timeMovie">
                            <p className="mDur">{movi.duration}</p>
                        </div>
                     </div>
                </div>))}
            </div>
        </div>
        </>
      )
}
export default Movies