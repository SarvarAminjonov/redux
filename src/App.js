import './App.css';
import Header from './Component/Header/header';
import Movies from './Component/MovieSection/movie';
import Comment from './Component/commentSection/comment';

function App() {
 
  return (
    <>
    <div className='container'>
    <Header/>
    <div className='flexBox'>
    <Movies/>
    <Comment/>
    </div>
    
    </div>
    
    </>
  );
}

export default App;
